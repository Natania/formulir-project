<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
   
Route::get('/formulir', 'API\FormulirController@index');
Route::post('/formulir', 'API\FormulirController@store');
Route::get('/formulir/{id}','API\FormulirController@show');
Route::put('/formulir/{id}','API\FormulirController@update');
Route::delete('/formulir/{id}','API\FormulirController@destroy');


