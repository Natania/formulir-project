<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use App\Models\Formulir;
use Exception;

class FormulirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $Formulir = Formulir::all();

            $response = $Formulir;
            $code = 200;
        }catch (\Exception $e){
            $code = 500;
            $response = $e->getMessage();
        }
        return apiResponseBuilder($code, $response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'tanggal_lahir'=>'required',
            'alamat_rumah'=>'required',
            'asal_sekolah'=>'required',
            'sekolah_lanjutan'=>'required',
            'image' => 'required | image | mimes:jpg,jpeg,png,gif',
            'file'=>'required',
            'description'=>'required',
            
        ]);

        try{
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'),$imageName);

            $Formulir = new Formulir();

            $Formulir->name = $request->name;
            $Formulir->tanggal_lahir = $request->tanggal_lahir;
            $Formulir->alamat_rumah= $request->alamat_rumah;
            $Formulir->asal_sekolah = $request->asal_sekolah;
            $Formulir->sekolah_lanjutan= $request->sekolah_lanjutan;
            $Formulir->image = $imageName;
            $Formulir->file = $request->file;
            $Formulir->description = $request->description;
           

            $Formulir->save();

            $code = 200;
            $response = $Formulir;

        }catch (\Exeption $e){
            if ($e instanceof ValidationException) {
                $code = 400;
                $response = 'tidak ada data';
            }else{
                $code = 500;
                $response = $e->getMessage(); 
            }
        }
        return apiResponseBuilder($code,$response);
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $Formulir = Formulir::findOrFail($id);

            $code = 200;
            $response = $Formulir;
        }catch (\Exception $e){
            if($e instanceof ModelNotFoundException){
                $code = 404;
                $response = 'inputkan sesuai id';
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }
        return apiResponseBuilder($code, $response); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required',
            'tanggal_lahir'=>'required',
            'alamat_rumah'=>'required',
            'asal_sekolah'=>'required',
            'sekolah_lanjutan'=>'required',
            'image' => 'required | image | mimes:jpg,jpeg,png,gif',
            'file'=>'required',
            'description'=>'required',
            
        ]);

        try{
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'),$imageName);

            $Formulir = new Formulir();

            $Formulir->name = $request->name;
            $Formulir->tanggal_lahir = $request->tanggal_lahir;
            $Formulir->alamat_rumah= $request->alamat_rumah;
            $Formulir->asal_sekolah = $request->asal_sekolah;
            $Formulir->sekolah_lanjutan= $request->sekolah_lanjutan;
            $Formulir->image = $imageName;
            $Formulir->file = $request->file;
            $Formulir->description = $request->description;
           

            $Formulir->save();

            $code = 200;
            $response = $Formulir;

        }catch (\Exeption $e){
            if ($e instanceof ValidationException) {
                $code = 400;
                $response = 'tidak ada data';
            }else{
                $code = 500;
                $response = $e->getMessage(); 
            }
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $Formulir = Formulir::find($id);
            $Formulir->delete();

            $code = 200;
            $response = $Formulir;
        }catch (\Exception $e){
            $code = 500;
            $response = $e->getMessage();
        }

        return apiResponseBuilder($code,$response);
    }
}
