<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormulirTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formulir', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',50);
            $table->string('tanggal_lahir');
            $table->string('alamat_rumah');
            $table->string('asal_sekolah');
            $table->string('sekolah_lanjutan');
            $table->string('image')->nullable();
            $table->string('file');
            $table->string('description',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formulir');
    }
}
